(function (describe, it) {
  'use strict';
  var assert = require('assert');
  var helper = require('../src/scripts/modules/helper.js');
  var model = require('../src/scripts/modules/model.js');

  model.menuItem = {
    'title': 'Super Burger',
    'count': 111,
    'id': 1
  };
  model.menu = [
    {
      'title': 'Super Burger',
      'count': 111,
      'id': 1
    },
    {
      'title': 'Mini Burger',
      'count': 222,
      'id': 2
    }
  ];
  describe('Helper tests', function () {
    describe('increment', function () {
      it('should increment the menu item', function () {
        helper.increment();
        assert.equal(model.menuItem.count, 112);
      });
    });
    describe('set current', function () {
      it('should set the current menu item', function () {
        helper.setCurrent(2);
        assert.equal(model.menuItem.id, 2);
      });
    });
    describe('get current', function () {
      it('should return the current menu item', function () {
        helper.setCurrent(1);
        helper.getCurrent();
        assert.equal(model.menuItem.id, 1);
        helper.setCurrent(2);
        helper.getCurrent();
        assert.equal(model.menuItem.id, 2);
      });
    });
  });
}(describe, it));