(function (module) {
  'use strict';
  var request = require('superagent');
  var model;
  model = module.exports = {
    init: function () {
      model.getData();
    },
    currentDate: new Date(),
    eventDate: new Date(2068, 6, 2),
    menuItem: {
      title: null,
      count: null,
      id: null
    },
    menu: {},
    getData: function () {
      request.get('menu.json', function (err, res) {
        // assign an Array of menu objects form a JSON to a menu
        model.menu = JSON.parse(res.text).menuItems;
        if (err) { console.log(err); }
      });
    }
  };
}(module));
