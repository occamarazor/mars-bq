(function (module) {
  'use strict';
  var countdown = require('countdown');
  var model = require('./model');
  var helper = module.exports = {
    dateDiff: function () {
      return countdown(model.currentDate, model.eventDate);
    },
    setCurrent: function (id) {
      model.menu.forEach(function (item) {
        // if id property of a menu object
        // matches data-id of the clicked li
        // assign the corresponding menu object to a menu Item
        if (item.id === id) {
          model.menuItem = item;
        }
      });
    },
    getCurrent: function () {
      return model.menuItem;
    },
    increment: function () {
      model.menuItem.count += 1;
    }
  };
}(module));
