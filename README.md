# A fun front-end tool up

Summary: exploring some JS tools with node environment.  
Just a welcome party on Mars with an event countdown & votes for the best menu.  
**Demo version: [Marsbq.io](https://marsbq-maxcrank.c9users.io/)**  
*Note: the app is available only when the server is running!*  
*Or you can install __node__ & __npm__ packages and run __gulp__. It will do the rest.*  
### What's inside  
1. Yeoman scaffolding
2. Gulp task running
3. Handlebars templates
4. Less preprocessing
5. Browserify modules
6. Native JS MVH code structure
7. Countdown, Superagent libs and normalize.css
8. JSHint linting
9. Browsersync device testing
10. Mocha unit testing
11. A11Y quality testing for accessibility